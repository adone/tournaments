package models

import (
	"time"

	"github.com/satori/go.uuid"
)

// Participation is a model of player data
type Participation struct {
	ID uuid.UUID

	// attributes
	PublicID  string
	Confirmed bool
	Winning   bool
	Prize     int

	// relations
	TournamentID uuid.UUID
	PlayerID     uuid.UUID

	Tournament   *Tournament
	Player       *Player
	Sponsorships []*Sponsorship

	// meta information
	BackersCount uint64

	CreatedAt time.Time
	UpdatedAt time.Time
}
