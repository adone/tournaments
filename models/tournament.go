package models

import (
	"time"
	"errors"

	"github.com/satori/go.uuid"
)

// Tournament is a model of tournament data
type Tournament struct {
	ID       uuid.UUID
	PublicID string

	// attributes
	Threshold uint64
	Deposit   uint64
	Finished  bool

	// relations
	Participations []*Participation

	// meta information
	ParticipantsCount int

	CreatedAt time.Time
	UpdatedAt time.Time
}

// TopUpDeposit adds threshold amount of points into deposit in case of new participation
func (tournament *Tournament) TopUpDeposit() {
	tournament.Deposit = tournament.Deposit + tournament.Threshold
}

// Finish finishes tournament
// returns error in case of already finished tournament
func (tournament *Tournament) Finish() error {
	if tournament.Finished {
		return errors.New("already finished")
	}

	tournament.Finished = true

	return nil
}
