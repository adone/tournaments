package models

import (
	"time"

	"github.com/satori/go.uuid"
)

// Sponsorship is a model of sponsorship data
type Sponsorship struct {
	ID uuid.UUID

	// attributes
	Accepted bool

	// relations
	ParticipationID uuid.UUID
	BackerID        uuid.UUID

	Backer *Player

	// meta information
	CreatedAt time.Time
	UpdatedAt time.Time
}
