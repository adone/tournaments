require 'erb'
require 'yaml'
require 'logger'
require 'dotenv'
require 'active_record'

ROOT = Pathname.new File.expand_path('..', __FILE__)

environment = ENV['ENV'] || 'development'

Dotenv.load(
  ROOT.join(".env.#{environment}.local"),
  ROOT.join(".env.local"),
  ROOT.join(".env.#{environment}"),
  ROOT.join(".env")
)

include ActiveRecord::Tasks

class Seeder
  def initialize(seed_file)
    @seed_file = seed_file
  end

  def load_seed
    raise "Seed file '#{@seed_file}' does not exist" unless File.file? @seed_file
    load @seed_file
  end
end

DatabaseTasks.env = environment
DatabaseTasks.database_configuration = YAML.load ERB.new(ROOT.join('config/database.yml').read).result
DatabaseTasks.db_dir = ROOT.join('db')
DatabaseTasks.migrations_paths = [ROOT.join('db/migrate')]
DatabaseTasks.seed_loader = Seeder.new ROOT.join('db/seeds.rb')
DatabaseTasks.root = ROOT

task :environment do
  ActiveRecord::Base.schema_format = :sql
  ActiveRecord::Base.configurations = DatabaseTasks.database_configuration
  ActiveRecord::Base.establish_connection DatabaseTasks.env.to_sym
end

namespace :generate do
  desc 'Create migration'
  task :migration, [:name] do |_, args|
    timestamp = Time.now.utc.strftime '%Y%m%d%H%M%S'

    path = DatabaseTasks.migrations_paths.first.join "#{timestamp}_#{args[:name].underscore}.rb"

    path.open File::CREAT|File::RDWR do |file|
      file.write <<~TEMPLATE
        class #{args[:name].camelcase} < ActiveRecord::Migration[#{ActiveRecord::Migration.current_version}]
          def change

          end
        end
      TEMPLATE
    end
  end
end

desc 'Satisfy dependencies'
task :deps do
  sh "cd #{ENV['GOPATH']}/src/tournaments && dep ensure -vendor-only"
end

desc 'Build application'
task :build do
  Rake::Task['build:app'].invoke
end

namespace :build do
  desc 'Build application'
  task :app do
    version = File.read 'VERSION'
    sh %{go build -ldflags '-X main.version=#{version}' -o bin/api tournaments/api}
  end

  desc 'Build Docker image'
  task :image do
    version = File.read 'VERSION'
    base = 'alpine3.7'
    commit = sh %{git log -n 1 --pretty=format:'%H'}

    tag = "#{version}-#{base}-#{commit}"

    sh %{docker build . --tag #{tag} --build-arg VERSION=#{version}}
  end
end

desc 'Run application'
task :run do
  Rake::Task['build:app'].invoke
end

namespace :run do
  desc 'Run application'
  task app: %w[build:app] do
    exec 'bin/api'
  end

  desc 'Run dockerized application'
  task :docker do
    sh %{docker-compose up}
  end
end

load 'active_record/railties/databases.rake'
