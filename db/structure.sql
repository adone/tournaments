SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: participations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE participations (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    tournament_id uuid NOT NULL,
    player_id uuid NOT NULL,
    public_id character varying NOT NULL,
    confirmed boolean DEFAULT false NOT NULL,
    winning boolean DEFAULT false NOT NULL,
    prize integer,
    backers_count integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: players; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE players (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    public_id character varying NOT NULL,
    points integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: sponsorships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sponsorships (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    participation_id uuid NOT NULL,
    backer_id uuid NOT NULL,
    accepted boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: tournaments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tournaments (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    public_id character varying NOT NULL,
    threshold integer DEFAULT 0 NOT NULL,
    deposit integer DEFAULT 0 NOT NULL,
    finished boolean DEFAULT false NOT NULL,
    participants_count integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: participations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_pkey PRIMARY KEY (id);


--
-- Name: players_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY players
    ADD CONSTRAINT players_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: sponsorships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sponsorships
    ADD CONSTRAINT sponsorships_pkey PRIMARY KEY (id);


--
-- Name: tournaments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tournaments
    ADD CONSTRAINT tournaments_pkey PRIMARY KEY (id);


--
-- Name: index_participations_on_player_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_participations_on_player_id ON participations USING btree (player_id);


--
-- Name: index_participations_on_public_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_participations_on_public_id ON participations USING btree (public_id);


--
-- Name: index_participations_on_tournament_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_participations_on_tournament_id ON participations USING btree (tournament_id);


--
-- Name: index_players_on_public_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_players_on_public_id ON players USING btree (public_id);


--
-- Name: index_sponsorships_on_backer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sponsorships_on_backer_id ON sponsorships USING btree (backer_id);


--
-- Name: index_sponsorships_on_participation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sponsorships_on_participation_id ON sponsorships USING btree (participation_id);


--
-- Name: index_tournaments_on_public_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tournaments_on_public_id ON tournaments USING btree (public_id);


--
-- Name: fk_rails_3b7f8216d1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT fk_rails_3b7f8216d1 FOREIGN KEY (tournament_id) REFERENCES tournaments(id) ON DELETE CASCADE;


--
-- Name: fk_rails_85d111e50f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sponsorships
    ADD CONSTRAINT fk_rails_85d111e50f FOREIGN KEY (participation_id) REFERENCES participations(id) ON DELETE CASCADE;


--
-- Name: fk_rails_bc6ba228b2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sponsorships
    ADD CONSTRAINT fk_rails_bc6ba228b2 FOREIGN KEY (backer_id) REFERENCES players(id) ON DELETE CASCADE;


--
-- Name: fk_rails_fa54aaa276; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT fk_rails_fa54aaa276 FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20180217230143');


