ARG GOVERSION=1.10
FROM golang:${GOVERSION}-alpine AS build

ARG DEPVERSION=0.4.1
ARG VERSION

RUN apk add --no-cache git curl make
RUN curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v${DEPVERSION}/dep-linux-amd64 && chmod +x /usr/local/bin/dep
RUN mkdir -p /go/src/tournaments
WORKDIR /go/src/tournaments

COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure -vendor-only

COPY . ./
RUN CGO_ENABLED=0 go build -ldflags '-X main.version=${VERSION} -w -extld ld -extldflags -static' -a -o bin/api tournaments/api

ARG BASE_IMAGE=alpine:3.7
FROM ${BASE_IMAGE}

WORKDIR /tournaments
COPY --from=build /go/src/tournaments/bin/api bin/api

ENTRYPOINT ["bin/api"]
