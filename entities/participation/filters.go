package participation

import (
	"github.com/go-pg/pg/orm"
	"github.com/satori/go.uuid"
)

// Filter adds customization for repository
type Filter struct {
	function func(*orm.Query) (*orm.Query, error)
}

// InTournament created participation filter for specific tournament
func InTournament(id uuid.UUID) Filter {
	return Filter{
		func(query *orm.Query) (*orm.Query, error) {
			return query.Where("tournament_id = ?", id), nil
		},
	}
}
