package participation

import (
	"github.com/satori/go.uuid"

	"tournaments/models"
)

// PlayerPreloader helps with avoidng N+1 queries for players
type PlayerPreloader interface {
	Load(playerID []uuid.UUID) ([]*models.Player, error)
}

// PreloadPlayers creates new option for repository read methods
func PreloadPlayers(preloader PlayerPreloader) Option {
	return Option{
		func(participations []*models.Participation) error {
			ids := make([]uuid.UUID, len(participations))
			mapping := make(map[uuid.UUID]*models.Participation)

			for index, participation := range participations {
				ids[index] = participation.PlayerID
				mapping[participation.PlayerID] = participation
			}

			players, err := preloader.Load(ids)
			if err != nil {
				return err
			}

			for _, player := range players {
				mapping[player.ID].Player = player
			}

			return nil
		},
	}
}
