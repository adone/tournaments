package participation

import (
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"

	"tournaments/models"
)

// Option is an option for repository methods
type Option struct {
	apply func(participations []*models.Participation) error
}

// NewRepository creates new participation repository with provided filters
// store can be pg.DB or pg.Tx to execute queries during a transaction
// use pg.Tx as store for select-for-update support
func NewRepository(store orm.DB, filters ...Filter) Repository {
	return Repository{store, filters}
}

// Repository is a Participation CRUD service
type Repository struct {
	store   orm.DB
	filters []Filter
}

// Create creates provided participation model
func (repository Repository) Create(participation *models.Participation) error {
	_, err := repository.store.Model(participation).Returning("*").Insert()

	return err
}

// Read reads one participation associated with provided public ID
func (repository Repository) Read(id string) (*models.Participation, error) {
	participation := &models.Participation{PublicID: id}

	query := repository.store.Model(participation).
		Where("public_id = ?public_id")

	for _, filter := range repository.filters {
		query = query.Apply(filter.function)
	}

	err := query.Select()

	return participation, err
}

// ReadMany reads many participations by their public ids
func (repository Repository) ReadMany(ids []string, options ...Option) ([]*models.Participation, error) {
	var participations []*models.Participation

	query := repository.store.Model(&participations).
		Where("public_id IN (?)", pg.In(ids))

	for _, filter := range repository.filters {
		query = query.Apply(filter.function)
	}

	err := query.Select()
	if err != nil {
		return participations, err
	}

	for _, option := range options {
		if err := option.apply(participations); err != nil {
			return participations, err
		}
	}

	return participations, nil
}

// ReadManyForUpdate reads participation models associated with provided public IDs
// and locks them by FOR UPDATE statement in transaction (if repository was created with pg.Tx)
func (repository Repository) ReadManyForUpdate(ids []string, options ...Option) ([]*models.Participation, error) {
	var participations []*models.Participation

	query := repository.store.Model(&participations).
		Where("public_id IN (?)", pg.In(ids)).For("UPDATE")

	for _, filter := range repository.filters {
		query = query.Apply(filter.function)
	}

	err := query.Select()
	if err != nil {
		return participations, err
	}

	for _, option := range options {
		if err := option.apply(participations); err != nil {
			return participations, err
		}
	}

	return participations, nil
}

// Update updates provided participation model
// columns for update can be customized
// default updatable columns are 'winning', 'prize' and 'updated_at'
func (repository Repository) Update(participation *models.Participation, columns ...string) error {
	participation.UpdatedAt = time.Now()

	if len(columns) == 0 {
		columns = append(columns, "winning", "prize", "updated_at")
	}

	_, err := repository.store.Model(participation).Column(columns...).Update()

	return err
}
