package sponsorship

import (
	"github.com/satori/go.uuid"

	"tournaments/models"
)

// Load loads sponsorships associated with provided ids
type Load func([]uuid.UUID, ...Option) ([]*models.Sponsorship, error)

// NewPreloader creates sponsorship preloader based on provided load function
func NewPreloader(load Load, options ...Option) Preloader {
	return Preloader{load, options}
}

// Preloader helps different repositories preload sponsorships
type Preloader struct {
	load    Load
	options []Option
}

// Load loads sponsorships associated with provided ids
func (preloader Preloader) Load(ids []uuid.UUID) ([]*models.Sponsorship, error) {
	return preloader.load(ids, preloader.options...)
}
