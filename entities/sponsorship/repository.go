package sponsorship

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/satori/go.uuid"

	"tournaments/models"
)

// Option is an option for repository methods
type Option struct {
	apply func(sponsorships []*models.Sponsorship) error
}

// NewRepository creates new sponsorship repository
// store can be pg.DB or pg.Tx to execute queries during a transaction
// use pg.Tx as store for select-for-update support
func NewRepository(store orm.DB) Repository {
	return Repository{store}
}

// Repository is a Sponsorship CRUD service
type Repository struct {
	store orm.DB
}

// CreateMany creates provided sponsorship models
func (repository Repository) CreateMany(sponsorships []*models.Sponsorship) error {
	_, err := repository.store.Model(&sponsorships).Returning("*").Insert()

	return err
}

// ReadManyByParticipationID reads sponsorship models associated with provided participation IDs
func (repository Repository) ReadManyByParticipationID(ids []uuid.UUID, options ...Option) ([]*models.Sponsorship, error) {
	var sponsorships []*models.Sponsorship

	err := repository.store.Model(&sponsorships).Where("participation_id IN (?)", pg.In(ids)).Select()

	if err != nil {
		return sponsorships, err
	}

	for _, option := range options {
		if err := option.apply(sponsorships); err != nil {
			return sponsorships, err
		}
	}

	return sponsorships, nil
}
