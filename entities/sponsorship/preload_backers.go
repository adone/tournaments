package sponsorship

import (
	"github.com/satori/go.uuid"

	"tournaments/models"
)

// BackerPreloader helps with avoidng N+1 queries for backers
type BackerPreloader interface {
	Load(playerID []uuid.UUID) ([]*models.Player, error)
}

// PreloadBackers creates new option for repository read methods
func PreloadBackers(preloader BackerPreloader) Option {
	return Option{
		func(sponsorships []*models.Sponsorship) error {
			ids := make([]uuid.UUID, len(sponsorships))
			mapping := make(map[uuid.UUID]*models.Sponsorship)

			for index, sponsorship := range sponsorships {
				ids[index] = sponsorship.BackerID
				mapping[sponsorship.BackerID] = sponsorship
			}

			backers, err := preloader.Load(ids)
			if err != nil {
				return err
			}

			for _, backer := range backers {
				mapping[backer.ID].Backer = backer
			}

			return nil
		},
	}
}
