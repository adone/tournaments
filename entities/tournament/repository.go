package tournament

import (
	"time"

	"github.com/go-pg/pg/orm"

	"tournaments/models"
)

// NewRepository creates new tournament repository
// store can be pg.DB or pg.Tx to execute queries during a transaction
// use pg.Tx as store for select-for-update support
func NewRepository(store orm.DB) Repository {
	return Repository{store}
}

// Repository is a Tournament CRUD service
type Repository struct {
	store orm.DB
}

// Create creates provided tournament model
func (repository Repository) Create(tournament *models.Tournament) error {
	_, err := repository.store.Model(tournament).Returning("*").Insert()

	return err
}

// Read reads one tournament associated with provided public ID
func (repository Repository) Read(id string) (*models.Tournament, error) {
	tournament := &models.Tournament{PublicID: id}

	err := repository.store.Model(tournament).Where("public_id = ?public_id").Select()

	return tournament, err
}

// ReadForUpdate reads tournament associated with provided public ID
// and locks row by FOR UPDATE statement in transaction (if repository was created with pg.Tx)
func (repository Repository) ReadForUpdate(id string) (*models.Tournament, error) {
	tournament := &models.Tournament{
		PublicID: id,
	}

	err := repository.store.Model(tournament).
		Where("public_id = ?public_id").For("UPDATE").Select()

	return tournament, err
}

// Update updates provided tournament model
// columns for update can be customized
// default updatable columns are 'deposit' and 'updated_at'
func (repository Repository) Update(tournament *models.Tournament, columns ...string) error {
	tournament.UpdatedAt = time.Now()

	if len(columns) == 0 {
		columns = append(columns, "deposit", "updated_at")
	}

	_, err := repository.store.Model(tournament).Column(columns...).Update()

	return err
}
