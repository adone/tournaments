package player

import (
	"tournaments/models"
)

// NewView creates player public representaion
func NewView(player *models.Player) View {
	return View{
		PlayerID: player.PublicID,
		Balance:  player.Points,
	}
}

// View is a player model public representaion
type View struct {
	PlayerID string `json:"playerId"`
	Balance  uint64 `json:"balance"`
}
