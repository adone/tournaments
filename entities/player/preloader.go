package player

import (
	"github.com/satori/go.uuid"

	"tournaments/models"
)

// Load loads players associated with provided ids
type Load func([]uuid.UUID) ([]*models.Player, error)

// NewPreloader creates player preloader based on provided load function
func NewPreloader(load Load) Preloader {
	return Preloader{load}
}

// Preloader helps different repositories preload players
type Preloader struct {
	load func([]uuid.UUID) ([]*models.Player, error)
}

// Load loads players associated with provided ids
func (preloader Preloader) Load(ids []uuid.UUID) ([]*models.Player, error) {
	return preloader.load(ids)
}
