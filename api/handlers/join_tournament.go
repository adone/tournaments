package handlers

import (
	"net/http"

	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"go.uber.org/zap"

	"tournaments/entities/participation"
	"tournaments/entities/player"
	"tournaments/entities/sponsorship"
	"tournaments/entities/tournament"
	"tournaments/models"
)

// NewJoinTournamentRequestHandler creates new JoinTournamentRequestHandler
// provided logger will be wrapped into 'join' namespace
func NewJoinTournamentRequestHandler(database *pg.DB, logger *zap.Logger) JoinTournamentRequestHandler {
	return JoinTournamentRequestHandler{BaseHandler{logger.Named("join")}, database}
}

// JoinTournamentRequestHandler handles player join request
type JoinTournamentRequestHandler struct {
	BaseHandler
	store *pg.DB
}

// JoinTournamentRequestParameters contains parameters for joining tournament
type JoinTournamentRequestParameters struct {
	TournamentID string   `query:"tournamentId" validate:"required"`
	PlayerID     string   `query:"playerId" validate:"required"`
	Backers      []string `query:"backerId"`
}

// Serve serves incoming requests
func (handler JoinTournamentRequestHandler) Serve(ctx echo.Context) error {
	params := new(JoinTournamentRequestParameters)

	if err := ctx.Bind(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return err
	}

	if err := ctx.Validate(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return handler.HandleValidationErrors(ctx, err)
	}

	transaction, err := handler.store.Begin()
	if err != nil {
		handler.logger.Error("creating transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	playerRepository := player.NewRepository(transaction)
	tournamentRepository := tournament.NewRepository(transaction)
	participationRepository := participation.NewRepository(transaction)
	sponsorhipRepository := sponsorship.NewRepository(transaction)

	playerModel, err := playerRepository.ReadForUpdate(params.PlayerID)
	if err != nil {
		handler.logger.Error("getting player has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	tournamentModel, err := tournamentRepository.ReadForUpdate(params.TournamentID)
	if err != nil {
		handler.logger.Error("getting tournament has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	participationModel := &models.Participation{
		Confirmed:    true,
		PublicID:     playerModel.PublicID,
		PlayerID:     playerModel.ID,
		TournamentID: tournamentModel.ID,
	}

	if err := participationRepository.Create(participationModel); err != nil {
		handler.logger.Error("creating participation has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	playerContribution := tournamentModel.Threshold

	tournamentModel.TopUpDeposit()

	if len(params.Backers) > 0 {
		backers, err := playerRepository.ReadManyForUpdate(params.Backers)
		if err != nil {
			handler.logger.Error("getting backers has failed", zap.Error(err))
			if err := transaction.Rollback(); err != nil {
				handler.logger.Error("transaction has failed", zap.Error(err))
			}

			return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
		}

		backersCount := uint64(len(backers))
		contribution := tournamentModel.Threshold / (backersCount + 1)
		playerContribution = contribution + (tournamentModel.Threshold - contribution*(backersCount+1))

		sponsorships := make([]*models.Sponsorship, len(backers))

		for index, backer := range backers {
			if err := backer.RemovePoints(contribution); err != nil {
				handler.logger.Error("updating backer balance has failed", zap.Error(err))
				if err := transaction.Rollback(); err != nil {
					handler.logger.Error("transaction has failed", zap.Error(err))
				}

				return echo.NewHTTPError(http.StatusBadRequest, "Something goes wrong")
			}

			sponsorships[index] = &models.Sponsorship{
				ParticipationID: participationModel.ID,
				BackerID:        backer.ID,
				Accepted:        true,
			}

			if err := playerRepository.Update(backer); err != nil {
				handler.logger.Error("updating backer has failed", zap.Error(err))
				if err := transaction.Rollback(); err != nil {
					handler.logger.Error("transaction has failed", zap.Error(err))
				}

				return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
			}
		}

		participationModel.BackersCount = backersCount

		if err := participationRepository.Update(participationModel); err != nil {
			handler.logger.Error("updating participation has failed", zap.Error(err))
			if err := transaction.Rollback(); err != nil {
				handler.logger.Error("transaction has failed", zap.Error(err))
			}

			return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
		}

		if err := sponsorhipRepository.CreateMany(sponsorships); err != nil {
			handler.logger.Error("creating sponsorships has failed", zap.Error(err))
			if err := transaction.Rollback(); err != nil {
				handler.logger.Error("transaction has failed", zap.Error(err))
			}

			return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
		}
	}

	if err := playerModel.RemovePoints(playerContribution); err != nil {
		handler.logger.Error("updating player balance has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusBadRequest, "Something goes wrong")
	}

	if err := playerRepository.Update(playerModel); err != nil {
		handler.logger.Error("updating player has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := tournamentRepository.Update(tournamentModel); err != nil {
		handler.logger.Error("updating tournament has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := transaction.Commit(); err != nil {
		handler.logger.Error("transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	return nil
}
