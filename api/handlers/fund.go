package handlers

import (
	"net/http"

	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"go.uber.org/zap"

	"tournaments/entities/player"
)

// NewFundRequestHandler creates new FundRequestHandler
// provided logger will be wrapped into 'fund' namespace
func NewFundRequestHandler(database *pg.DB, logger *zap.Logger) FundRequestHandler {
	return FundRequestHandler{BaseHandler{logger.Named("fund")}, database}
}

// FundRequestHandler funds players with provided amount of points
type FundRequestHandler struct {
	BaseHandler
	store *pg.DB
}

// FundRequestParameters contains parameters
type FundRequestParameters struct {
	PlayerID string `query:"playerId" validate:"required"`
	Points   uint64 `query:"points" validate:"gt=0"`
}

// Serve serves incoming requests
func (handler FundRequestHandler) Serve(ctx echo.Context) error {
	params := new(FundRequestParameters)

	if err := ctx.Bind(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return err
	}

	if err := ctx.Validate(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return handler.HandleValidationErrors(ctx, err)
	}

	transaction, err := handler.store.Begin()
	if err != nil {
		handler.logger.Error("creating transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	playerRepository := player.NewRepository(transaction)

	player, err := playerRepository.ReadForUpdate(params.PlayerID)
	player.AddPoints(params.Points)

	if err != nil {
		switch err {
		case pg.ErrNoRows:
			err = playerRepository.Create(player)
			fallthrough
		case nil:
			// do nothing
		default:
			handler.logger.Error("creating player has failed", zap.Error(err))
			if err := transaction.Rollback(); err != nil {
				handler.logger.Error("transaction has failed", zap.Error(err))
			}

			return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
		}
	} else {
		if err := playerRepository.Update(player); err != nil {
			handler.logger.Error("saving player has failed", zap.Error(err))
			if err := transaction.Rollback(); err != nil {
				handler.logger.Error("transaction has failed", zap.Error(err))
			}

			return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
		}
	}

	if err := transaction.Commit(); err != nil {
		handler.logger.Error("transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	return nil
}
