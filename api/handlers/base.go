package handlers

import (
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"go.uber.org/zap"
)

// BaseHandler can handler validation errors
type BaseHandler struct {
	logger *zap.Logger
}

// HandleValidationErrors humanizes validation errors
func (handler BaseHandler) HandleValidationErrors(ctx echo.Context, err error) error {
	details := make(map[string]string)

	for _, fieldError := range err.(validator.ValidationErrors) {
		var description string

		switch fieldError.Tag() {
		case "min":
			description = "must contain minimum " + fieldError.Param()
		case "gt":
			description = "must be greater than " + fieldError.Param()
		default:
			description = fieldError.Tag()
		}
		details[fieldError.Field()] = description
	}

	return ctx.JSON(http.StatusBadRequest, details)
}
