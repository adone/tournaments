package main

import (
	"github.com/go-playground/validator"
)

// Validator integrates go-playground/validator into echo web server
type Validator struct {
	validator *validator.Validate
}

// Validate validates provided params
func (wrapper Validator) Validate(data interface{}) error {
	return wrapper.validator.Struct(data)
}
